import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FirstFragment extends StatefulWidget {
  @override
  _FirstFragmentState createState() => _FirstFragmentState();
}

class _FirstFragmentState extends State<FirstFragment> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(12.0),
      child: new Card(
        child: new Center(
            child: new Icon(
          Icons.sentiment_very_satisfied,
          size: 128.00,
        )),
      ),
    );
  }
}
