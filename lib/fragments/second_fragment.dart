import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SecondFragment extends StatefulWidget {
  @override
  _SecondFragmentState createState() => _SecondFragmentState();
}

class _SecondFragmentState extends State<SecondFragment> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(12.0),
      child: new Card(
        child: new Center(
            child: new Icon(
          Icons.school,
          size: 128.00,
        )),
      ),
    );
  }
}
